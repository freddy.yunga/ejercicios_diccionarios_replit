"""
The text is given in a single line.
For each word of the text count the number of its occurrences before it.
"""

num= [int(s) for s in input().split()]
# Print a value:
# print(a)
for i in range(1, len(num)):
   if num[i] > num[ i-1]:
     print(num[i], end= ' ')
