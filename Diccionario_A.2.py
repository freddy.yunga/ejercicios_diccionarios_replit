"""
You are given a dictionary consisting of word pairs.
Every word is a synonym of the other word in its pair.
All the words in the dictionary are different.

After the dictionary there's one more word given. Find a synonym for it.

"""

n = input()
list_n = list(map(int, n.split()))
i = 1
for i in range(1,len(list_n)):
  if list_n[i]*list_n[i-1]>0:
    print(str(list_n[i - 1])+' '+str(list_n[i]))
    break
  elif i == len(list_n)-1:
    print("0")
