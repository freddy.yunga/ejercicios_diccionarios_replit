"""
The first line contains the number of records. After that,
each entry contains the name of the candidate and the number of votes they got in some state.
Count the results of the elections:
sum the number of votes for each candidate. Print candidates in the alphabetical order.
"""

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

x = input()
list_x = list(map(int, x.split()))
tally = 0

for n in range(1,len(list_x)-1):
  if list_x[n-1] < list_x[n] and list_x[n] > list_x[n+1]:
    tally = tally + 1
print(tally)
