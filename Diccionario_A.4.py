"""
Given the text: the first line contains the number of lines,
then given the lines of words. Print the word in the text that occurs most often.
If there are many such words, print the one that is less in the alphabetical order.
"""

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
x = input()
lst = list(map(int, x.split()))
total = 1

for i in range(1, len(lst)):
    if lst[i - 1] != lst[i]:
        total += 1

print(total)
