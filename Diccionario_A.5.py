"""
The virus attacked the filesystem of the supercomputer and broke the control of access rights to the files.
For each file there is a known set of operations which may be applied to it:

write W,
read R,
execute X.

The first line contains the number N — the number of files contained in the filesystem.
The following N lines contain the file names and allowed operations with them, separated by spaces.
The next line contains an integer M — the number of operations to the files.
In the last M lines specify the operations that are requested for files.
One file can be requested many times.

You need to recover the control over the access rights to the files.
For each request your program should return OK if the requested operation
is valid or Access denied if the operation is invalid.

"""
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
x = input()
lst = list(map(int, x.split()))

for i in range(0, len(lst), 2):
    popval = lst.pop(i)
    lst.insert(i + 1, popval)

print(lst)
