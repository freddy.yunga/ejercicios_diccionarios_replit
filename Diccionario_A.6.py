"""
Given a list of countries and cities of each country,
then given the names of the cities.
For each city print the country in which it is located.
"""



# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

Capitals = dict()

# Fill it with some values
Capitals['Russia'] = 'Moscow'
Capitals['Ukraine'] = 'Kiev'
Capitals['USA'] = 'Washington'

Countries = ['Russia', 'France', 'USA', 'Russia']

for country in Countries:
  # For each country from the list check to see whether it is in the dictionary Capitals
    if country in Capitals:
        print('The capital of ' + country + ' is ' + Capitals[country])
    else:
        print('The capital of ' + country + ' is unknown')







"""x = input()
lst = list(map(int, x.split()))
total = 1

for i in range(1, len(lst)):
  if lst[i-1] != lst[i]:
    total += 1
print(total)
"""


