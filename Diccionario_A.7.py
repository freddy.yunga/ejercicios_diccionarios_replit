"""
Given a number n, followed by n lines of text, print all words encountered in the text,
one per line, with their number of occurrences in the text.
The words should be sorted in descending order according to their number of occurrences,
and all words within the same frequency should be printed in lexicographical order.

Hint. After you created a dictionary of the words and their frequencies,
you would like to sort it according to the frequencies.
This can be achieved if you create a list whose elements are lists of two elements: the number of occurrences of a word and the word itself.
For example, [[2, 'hi'], [1, 'what'], [3, 'is']].
Then the standard list sort will sort a list of lists, with the lists compared by the first element,
and if these are equal, by the second element. This is nearly what is required.

"""

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
x = input()
lst = list(map(int, x.split()))

for i in range(0, len(lst), 2):
    popval = lst.pop(i)
    lst.insert(i + 1, popval)

print(lst)
